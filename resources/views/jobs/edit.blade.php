@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><h4>Edit Job</h4></div>
                    <div class="panel-body">
                        <form action="{{route('jobs.update', $job->id)}}" method="POST" class="form-horizontal">
                            {{method_field('PUT')}}
                            @include('jobs.partials.form')
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>

    <script>tinymce.init({ selector:'textarea', height: 250 });</script>
@endsection