<div class="form-body">
    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
        <label class="col-md-3 control-label">Title</label>
        <div class="col-md-6">
            <input type="text" name="title" value="{{old('title', isset($job) ? $job->title:null)}}" class="form-control" placeholder="Enter text">
            @if ($errors->has('title'))
            <span class="help-block">
                <strong>{{ $errors->first('title') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Email Address</label>
        <div class="col-md-6">
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="glyphicon glyphicon-envelope"></i>
                </span>
                <input type="text" class="form-control" name="email" value="{{old('email', isset($job) ? $job->email:null)}}" placeholder="Email Address">
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Description</label>
        <div class="col-md-9">
            <textarea class="summernote" name="description">{!! old('description', isset($job) ? $job->description : null) !!}</textarea>
        </div>
    </div>

    <div class="form-actions fluid">
        <div class="row">
            <div class="col-md-offset-3 col-md-9">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{route('jobs.index')}}" class="btn btn-default">Cancel</a>
                <span class="pull-right"> <label class="label label-info">Info!!</label> All fields are required.</span>
            </div>
        </div>
    </div>
</div>
{{csrf_field()}}
