@if (userIsModerator() && ($job->status === \App\Support\Constants::$job_submitted_status))
    <form action="{{route('jobs.approve', $job->id)}}" method="POST" style="display: inline" onsubmit="return confirm('Are you sure?')">
        {{csrf_field()}}
        <button type="submit" class="btn btn-success btn-sm">Approve</button>
    </form>

    <form action="{{route('jobs.spam', $job->id)}}" method="POST" style="display: inline" onsubmit="return confirm('Are you sure?')">
        {{csrf_field()}}
        <button type="submit" class="btn btn-danger btn-sm">Mark as Spam</button>
    </form>
@endif

@if(userIsManager() && in_array($job->status, \App\Support\Constants::$can_delete_status_list))
    <form action="{{route('jobs.destroy', $job->id)}}" method="POST" style="display: inline" onsubmit="return confirm('Are you sure?')">
        {{csrf_field()}}
        {{method_field('DELETE')}}
        <button type="submit" class="btn btn-danger btn-sm">Delete</button>
    </form>
@endif


@if($job->status == \App\Support\Constants::$job_draft_status)
    <a class="btn btn-primary" href="{{route('jobs.edit', $job->id)}}">Edit</a>
    <form action="{{route('jobs.submit', $job->id)}}" method="POST" style="display: inline" onsubmit="return confirm('Are you sure?')">
        {{csrf_field()}}
        <button type="submit" class="btn btn-success btn-sm">Submit</button>
    </form>
@endif

<a class="btn btn-default" href="{{route('jobs.index')}}">Back</a>