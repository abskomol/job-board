@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="list-group">
                    <form role="search" class="list-group-item">
                        <div class="form-group">
                            <div class="col-md-8">
                                <input type="text" name="filter[q]" value="{{isset($filter['q'])?$filter['q']:null}}" class="form-control" placeholder="Filter title, status">
                            </div>
                            <div class="col-md-2">
                                <button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-search"></i></button>
                                <a href="{{route('jobs.index')}}" class="btn btn-default"><i class="glyphicon glyphicon-refresh"></i></a>
                            </div>

                            @if (userIsManager())
                            <div class="col-md-2">
                                <a class="btn btn-primary pull-right" title="Create new job" href="{{route('jobs.create')}}">Create New</a>
                            </div>
                            @endif
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="list-group">
                    @forelse($jobs as $job)
                        <a href="{{route('jobs.details', [$job->id, $job->slug])}}" class="list-group-item">
                            {!! $job->statusString() !!}
                            <h4 class="list-group-item-heading"><strong>{{$job->title}}</strong></h4>

                            <p class="list-group-item-text">
                                <i class="glyphicon glyphicon-envelope"></i> {{$job->email}}
                                | <i class="glyphicon glyphicon-calendar"></i> Posted: {{$job->created_at->diffForHumans()}}
                                @if ($job->status === \App\Support\Constants::$job_approved_status)
                                | <i class="glyphicon glyphicon-ok"></i> Approved: {{$job->approved_date->diffForHumans()}}
                                @endif
                            </p>
                        </a>
                    @empty
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading"><strong>No job have found.</strong></h4>
                        </div>
                    @endforelse

                    @if ($jobs->count())
                    <div class="list-group-item">
                        <div class="pull-right">{{$jobs->render()}}</div>
                        <div class="clearfix"></div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection