@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3> {{$job->title}}
                            <div class="btn-group-sm pull-right">
                                {{--Moderator--}}
                                @include('jobs.partials.actions')
                            </div>
                        </h3>
                    </div>

                    <div class="panel-body">
                        <div class="row">

                            <div class="col-md-3">
                                <ul class="list-group">
                                    <li class="list-group-item">
                                        <span class="pull-right">{{$job->created_at->format(config('app.date_format'))}}</span>
                                        Posted At
                                    </li>
                                    <li class="list-group-item">
                                        {!! $job->statusString() !!}
                                        Status
                                    </li>
                                    @if ($job->status === \App\Support\Constants::$job_approved_status)
                                    <li class="list-group-item">
                                        <span class="label label-info pull-right">{{$job->approved_date->format(config('app.date_format'))}}</span>
                                        Approved At
                                    </li>
                                    @endif
                                    <li class="list-group-item">
                                        <span class="pull-right"><i class="glyphicon glyphicon-envelope"></i></span>
                                        {{$job->email}}
                                    </li>
                                </ul>
                            </div>

                            <div class="col-md-9">
                                {!! nl2br($job->description) !!}
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection