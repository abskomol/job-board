@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3> {{$job->title}}

                            <div class="btn-group-sm pull-right">
                                <a class="btn btn-primary" href="mailto:{{$job->email}}?subject={{$job->title}}">Apply Job!</a>
                                <a class="btn btn-default" href="{{url('/')}}">Back</a>
                            </div>
                        </h3>
                    </div>

                    <div class="panel-body">
                        <div class="row">

                            <div class="col-md-3">
                                <ul class="list-group">
                                    <li class="list-group-item">
                                        <span class="label label-info pull-right">{{$job->approved_date->format('M d, Y H:i:s')}}</span>
                                        Posted At
                                    </li>
                                    <li class="list-group-item">
                                        <span class="pull-right"><i class="glyphicon glyphicon-envelope"></i></span>
                                        {{$job->email}}
                                    </li>
                                </ul>
                            </div>

                            <div class="col-md-9">
                                {!! nl2br($job->description) !!}
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection