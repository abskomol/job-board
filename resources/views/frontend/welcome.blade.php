@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="list-group">
                    <form role="search" class="list-group-item">
                        <div class="form-group">
                            <div class="col-md-8">
                                <input name="filter[title]" value="{{isset($filter['title'])?$filter['title']:null}}" type="text" class="form-control" placeholder="Filter title!">
                            </div>
                            <div class="col-md-2">
                                <button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-search"></i></button>
                                <a href="{{url('/')}}" class="btn btn-default"><i class="glyphicon glyphicon-refresh"></i></a>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="list-group">
                    @forelse($jobs as $job)
                        <a href="{{route('public.job.details', [$job->id, $job->slug])}}" class="list-group-item">
                            <span class="pull-right">
                                {{$job->updated_at->diffForHumans()}}
                            </span>

                            <h4 class="list-group-item-heading"><strong>{{$job->title}}</strong></h4>

                            <p class="list-group-item-text">{{$job->email}}</p>
                        </a>
                    @empty
                        <div class="list-group-item">
                            No job have found.
                        </div>
                    @endforelse

                    @if(count($jobs))
                        <div class="list-group-item">
                            <div class="pull-right">{{$jobs->render()}}</div>
                            <div class="clearfix"></div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection