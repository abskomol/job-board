@extends('layouts.email')

@section('content')
    <p>Dear Manager,</p>
    <p>Your submission is in moderation.</p>
    <p>
        BR,<br/>
        {{config('app.name')}} Team
    </p>
@endsection
