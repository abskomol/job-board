@extends('layouts.email')

@section('content')
    <p>Dear Moderator,</p>
    <p>New job has been waiting for your moderation. Job information given below:</p>
    <p>
        <strong>Title :</strong> {{$jobObject->title}} <br>
        <strong>Description: </strong>
        {!! $jobObject->description !!}
    </p>
    <table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
        <tbody>
        <tr>
            <td align="left">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tbody>
                    <tr>
                        <td> <a href="{{ route('approve.job', ['id' => $jobObject->id, 'token' => $token]) }}" target="_blank">Approve</a> </td>
                        <td><a href="{{ route('spam.job', ['id' => $jobObject->id, 'token' => $token]) }}" target="_blank">Mark as Spam</a> </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
    <p>Good luck! Hope it works.</p>
@endsection