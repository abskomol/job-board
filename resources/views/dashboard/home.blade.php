@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Welcome</div>

                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-6">
                                <ul class="list-group">

                                    @if (userIsModerator())
                                        <a href="{{route('jobs.index', ['filter' => ['q' => \App\Support\Constants::$job_submitted_status]])}}" class="list-group-item">
                                            <span class="badge">{{$stats}}</span>
                                            Job Waiting for approval
                                        </a>
                                    @else
                                        <a href="{{route('jobs.index')}}" class="list-group-item">
                                            <span class="badge">{{$stats['total_submitted']}}</span>
                                            My total Job(s)
                                        </a>

                                        <a href="{{route('jobs.index', ['filter' => ['q' => \App\Support\Constants::$job_approved_status]])}}" class="list-group-item">
                                            <span class="badge">{{$stats['total_approved']}}</span>
                                            My approved Job(s)
                                        </a>

                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
