<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Job::class, function (Faker\Generator $faker) {
    $keys = \App\Support\Constants::$status_list;

    unset($keys['APPROVED']);

    $user = App\User::where('email', 'manager@test.com')->first();
    $title = $faker->sentence;

    return [
        'title' => $title,
        'slug' => str_slug($title),
        'email' => $faker->unique()->safeEmail,
        'description' => $faker->paragraph,
        'user_id' => $user->id,
        'status' => $faker->randomKey($keys)
    ];
});
