<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $moderator = [
            'name' => 'Job Moderator',
            'email' => 'moderator@test.com',
            'password' => bcrypt(123456),
            'auth_token' => Str::random(60),
            'is_moderator' => true
        ];

        $manager = [
            'name' => 'HR Manager',
            'email' => 'manager@test.com',
            'password' => bcrypt(123456),
            'auth_token' => Str::random(60),
            'is_manager' => true
        ];

        \App\User::create($manager);
        \App\User::create($moderator);
    }
}
