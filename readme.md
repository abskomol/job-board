Job Board Project using Laravel 5.3 Framework
==============

## Requirements

- PHP >= 5.6.4
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension


## How to install

        git clone https://gitlab.com/abskomol/job-board.git

        cd job-board

        composer install
        
## Setup your database.
Open .env file, change DB_* configs according to your environment.

    
## Migration

       php artisan migrate
       

## Seed
       
       php artisan db:seed


## Email
Open .env file, change EMAIL_* configs according to your environment
 
## Queue
Run following command in terminal. Otherwise you will not get any email.
  
     php artisan queue:work
     

Now login with
 
      HR Manager: manager@test.com / 123456
      Moderator: moderator / 123456
