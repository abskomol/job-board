<?php

Auth::routes();

// Public routes
Route::get('/', 'PublicController@index');

Route::group(['prefix' => 'job'], function () {
    Route::get('/{id}/approve', ['as' => 'approve.job', 'uses' => 'PublicController@approve'])
        ->middleware('auth.token');
    Route::get('/{id}/spam', ['as' => 'spam.job', 'uses' => 'PublicController@spam'])
        ->middleware('auth.token');

    Route::get('/{id}/{slug}', ['as' => 'public.job.details', 'uses' => 'PublicController@show']);

});

// After login routes
Route::get('/home', 'HomeController@index');
Route::resource('/jobs', 'JobsController', ['except' => ['show']]);

Route::group(['prefix' => 'jobs'], function () {
    Route::get('/{id}/{slug}', ['as' => 'jobs.details', 'uses' => 'JobsController@show']);
    Route::post('/{id}/submit', ['as' => 'jobs.submit', 'uses' => 'JobsController@submit']);
    Route::post('/{id}/approve', ['as' => 'jobs.approve', 'uses' => 'JobsController@approve']);
    Route::post('/{id}/spam', ['as' => 'jobs.spam', 'uses' => 'JobsController@spam']);
});
