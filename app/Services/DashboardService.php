<?php

namespace App\Services;

use App\Repositories\JobsRepository;
use App\Support\Constants;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DashboardService
{
    /**
     * @var \App\Repositories\JobsRepository
     */
    private $repository;

    /**
     * @param \App\Repositories\JobsRepository $repository
     */
    public function __construct(JobsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getStatus () {
        $query = $this->repository->getQuery();

        if (userIsManager()) {
           return $this->getManagerStats($query)->first();
        }

        return $this->getModeratorStats($query);
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    private function getManagerStats($query)
    {
        return $query->select('user_id',
                DB::raw('COUNT(id) AS total_submitted'),
                DB::raw('SUM(CASE WHEN status= "'.Constants::$job_approved_status.'" THEN 1 ELSE 0 END) AS total_approved')
            )
            ->where('user_id', Auth::id())
            ->groupBy('user_id');
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    private function getModeratorStats($query)
    {
        return $query
            ->where('status', Constants::$job_submitted_status)
            ->groupBy('status')
            ->count();
    }
}