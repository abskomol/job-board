<?php

namespace App\Services;

use App\Exceptions\JobStatusException;
use App\Job;
use App\Jobs\SendNotificationEmail;
use App\Repositories\JobsRepository;
use App\Support\Constants;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Class JobsService
 *
 * @package App\Services
 */
class JobsService
{
    /**
     * @var \App\Repositories\JobsRepository
     */
    private $jobsRepository;

    /**
     * @param \App\Repositories\JobsRepository $jobsRepository
     */
    public function __construct(JobsRepository $jobsRepository)
    {
        $this->jobsRepository = $jobsRepository;
    }

    /**
     * @param array $filter
     *
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function all(array $filter)
    {
        return $this->jobsRepository->getFilterWithPaginatedData($filter);
    }

    /**
     * Find By slug
     *
     * @param $slug
     *
     * @return bool|mixed
     */
    public function findBySlug($slug)
    {
        try {
            return $this->jobsRepository->findBy('slug', $slug);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param array $filter
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getPublishedJobs(array $filter)
    {
        return $this->jobsRepository->getPublishedJob($filter);
    }

    /**
     * Create new job
     *
     * @param array $data
     *
     * @return mixed
     */
    public function store(array $data)
    {
        return $this->jobsRepository->create($data);
    }

    /**
     * Find job by is and slug
     *
     * @param $id
     * @param $slug
     *
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public function findByIdAndSlug($id, $slug)
    {
        $query = $this->jobsRepository->getQuery();

        return $query->where('id', $id)
            ->where('slug', $slug)
            ->first();
    }

    /**
     * Find user job by id
     *
     * @param $id
     *
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public function findUserJobById($id)
    {
        $query = $this->jobsRepository->getQuery();

        return $query->where('id', $id)
            ->where('user_id', auth()->user()->id)
            ->first();
    }

    /**
     * Change status
     *
     * @param \App\Job $job
     *
     * @return array
     */
    public function submitJobForModeration(Job $job)
    {
        if ($job->status === Constants::$job_draft_status) {
            $job->status = Constants::$job_submitted_status;
            $job->save();


            /*Mail::to('moderator@test.com')
                ->send(new JobPosted($job));*/

            dispatch(new SendNotificationEmail($job));

            return ['message' => 'Job submitted for moderation.', 'type' => 'info'];
        }

        return ['message' => 'You already submitted this job. Current status is ' . $job->status, 'type' => 'error'];
    }

    /**
     * Delete job
     *
     * @param $id
     *
     * @return bool
     */
    public function deleteJob($id)
    {
        try {
            $job = $this->jobsRepository->find($id);
            $this->isEligibleToDelete($job);

            $job->delete();
            return true;
        } catch (JobStatusException $e) {
            return false;
        } catch (ModelNotFoundException $e) {
            return false;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Check eligibility for delete
     *
     * @param $job
     *
     * @return bool
     * @throws \App\Exceptions\JobStatusException
     */
    public function isEligibleToDelete($job)
    {
        if (in_array($job->status, Constants::$can_delete_status_list)) {
            return true;
        }

        throw new JobStatusException;
    }


    /**
     * Check eligibility for edit
     *
     * @param $job
     *
     * @return bool
     * @throws \App\Exceptions\JobStatusException
     */
    public function isEligibleToEdit($job)
    {
        if (in_array($job->status, Constants::$can_edit_status_list)) {
            return true;
        }

        throw new JobStatusException;
    }

    /**
     * Update job
     *
     * @param array $data
     * @param       $job
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(array $data, $job)
    {
        try {
            $this->isEligibleToEdit($job);
            return $this->jobsRepository->update($data, $job->id);
        } catch (JobStatusException $e) {
            return false;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Approve job
     *
     * @param \App\Job $job
     *
     * @return array
     */
    public function approveJob(Job $job)
    {
        if (userIsModerator() && $job->status === Constants::$job_submitted_status) {
            $this->changeStatusToApprove($job);

            return ['message' => 'Job approved.', 'type' => 'info'];
        }

        return ['message' => 'You don\'t have permission to approve this job.', 'type' => 'error'];
    }

    /**
     * Find by id
     *
     * @param $id
     *
     * @return bool|mixed
     */
    public function findById($id)
    {
        try {
            return $this->jobsRepository->find($id);
        } catch (ModelNotFoundException $e) {
            return false;
        }
    }

    /**
     * Mark job as spam
     *
     * @param $job
     *
     * @return array
     */
    public function markAsSpamJob($job)
    {
        if (userIsModerator() && $job->status === Constants::$job_submitted_status) {
            $this->changeStatusToSpam($job);
            return ['message' => 'Job marked as spam.', 'type' => 'info'];
        }

        return ['message' => 'You don\'t have permission to update this job.', 'type' => 'error'];
    }

    /**
     * @param $job
     *
     * @return array
     */
    public function approveJobFromMail(Job $job)
    {
        if ($job->status === Constants::$job_submitted_status) {
            $this->changeStatusToApprove($job);
            return ['message' => 'Job approved.', 'type' => 'info'];
        }

        return ['message' => 'This job already approved.', 'type' => 'error'];
    }

    /**
     * @param \App\Job $job
     *
     * @return array
     */
    public function markJobAsSpamFromMail(Job $job)
    {
        if ($job->status === Constants::$job_submitted_status) {
            $this->changeStatusToSpam($job);
            return ['message' => 'Job marked as spam.', 'type' => 'info'];
        }

        return ['message' => 'Failed to mark as spam. Job status is ' . $job->status, 'type' => 'error'];
    }


    /**
     * @param \App\Job $job
     */
    public function changeStatusToSpam(Job $job)
    {
        $job->status = Constants::$job_spam_status;
        $job->save();
    }

    /**
     * Change status to approve
     *
     * @param \App\Job $job
     */
    public function changeStatusToApprove(Job $job)
    {
        $job->status = Constants::$job_approved_status;
        $job->approved_date = Carbon::now();
        $job->save();
    }
}
