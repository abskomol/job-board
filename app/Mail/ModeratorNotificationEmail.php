<?php

namespace App\Mail;

use App\Job;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ModeratorNotificationEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * @var \App\Job
     */
    public $jobObject;

    /**
     * Create a new message instance.
     *
     * @param \App\Job $jobObject
     */
    public function __construct(Job $jobObject)
    {
        $this->jobObject = $jobObject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $moderator = User::moderator()->first();

        return $this->view('emails.moderator')
            ->withToken($moderator->auth_token)
            ->subject('New job has been posted');
    }
}
