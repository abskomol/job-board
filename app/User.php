<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'is_moderator', 'is_manager'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function jobs()
    {
        return $this->hasMany(Job::class, 'user_id');
    }

    /**
     * Scope a query to only include moderator users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeModerator($query)
    {
        return $query->where('is_moderator', 1);
    }

    /**
     * Scope a query to only include manager users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeManager($query)
    {
        return $query->where('is_manager', 1);
    }

    /**
     * Scope a query to only include token users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @param                                       $token
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfToken($query, $token)
    {
        return $query->where('auth_token', $token);
    }

    /**
     * @return bool
     */
    public function isModerator()
    {
        return (bool) $this->is_moderator;
    }
}
