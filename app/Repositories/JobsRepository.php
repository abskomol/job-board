<?php

namespace App\Repositories;

use App\Job;
use App\Support\Constants;

/**
 * Class JobsRepository
 *
 * @package App\Repositories
 */
class JobsRepository extends Repository
{
    public $recordPerPage = 10;

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return Job::class;
    }

    /**
     * Filter data based on user input
     *
     * @param array $filter
     * @param       $query
     */
    public function filterData(array $filter, $query)
    {
        if (userIsManager()) {
            $this->addUserQuery($query);
        }

        if (isset($filter['q'])) {
            $query->where(function ($subQ) use ($filter){
                $subQ->orWhere('title', 'LIKE', "%{$filter['q']}%");
                $subQ->orWhere('status', '=', $filter['q']);
            });
        }
    }

    /**
     * Filter user post if logged in user is manager
     *
     * @param $query
     *
     * @return null
     */
    protected function addUserQuery($query)
    {
        $query->where('user_id', auth()->user()->id);
    }

    /**
     * @param $query
     * @param $filter
     */
    protected function addDraftStatusQuery($query, $filter)
    {
        if (empty($filter) && $this->userIsManager()) { //Default filter
            $query->where('status', Constants::$job_draft_status);
        }
    }

    /**
     * @param $query
     * @param $filter
     */
    protected function addSubmittedQuery($query, $filter)
    {
        if (empty($filter)) { //Default filter
            $query->where('status', Constants::$job_submitted_status);
        }
    }

    /**
     * @return bool
     */
    public function userIsManager()
    {
        if (!auth()->check()) {
            return false;
        }

        $user = auth()->user();

        return (boolean) $user->is_manager;
    }

    public function getPublishedJob(array $filter)
    {
        $query = $this->getQuery();
        $query->where('status', Constants::$job_approved_status);
        $query->orderBy('approved_date', 'DESC');

        if (isset($filter['title'])) {
            $query->where('title', 'LIKE', "%{$filter['title']}%");
        }

        return $query->paginate($this->recordPerPage);
    }
}
