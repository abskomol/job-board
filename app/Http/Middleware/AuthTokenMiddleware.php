<?php

namespace App\Http\Middleware;

use App\Exceptions\AuthenticationTokenException;
use App\User;
use Closure;

class AuthTokenMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->authenticate($request->get('token'));

        return $next($request);
    }

    protected function authenticate($token)
    {
        if ($token) {
            $user = User::ofToken($token)->first();

            if ($user) {
                return true;
            }
        }

        throw new AuthenticationTokenException('Unauthenticated.');
    }
}
