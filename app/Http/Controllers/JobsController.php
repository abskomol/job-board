<?php

namespace App\Http\Controllers;

use App\Http\Requests\JobRequest;
use App\Services\JobsService;
use Illuminate\Http\Request;

class JobsController extends Controller
{
    /**
     * @var \App\Services\JobsService
     */
    private $jobsService;

    /**
     * @param \App\Services\JobsService $jobsService
     */
    public function __construct(JobsService $jobsService)
    {
        $this->middleware('auth');
        $this->jobsService = $jobsService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = $request->get('filter', []);

        $jobs = $this->jobsService->all($filter);

        return view('jobs.index', [
            'jobs' => $jobs,
            'filter' => $filter
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('jobs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\JobRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(JobRequest $request)
    {
        $job = $this->jobsService->store($request->all());

        if (!$job) {
            flash('Failed to create job', 'error');
            return redirect()->back()->withInput();
        }

        flash('Job created successfully. Please submit this if you are done.');

        return redirect()->route('jobs.details', [$job->id, $job->slug]);
    }

    /**
     * Display the specified resource.
     *
     * @param         $id
     * @param  string $slug
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id, $slug)
    {
        $job = $this->jobsService->findByIdAndSlug($id, $slug);

        if (!$job) {
            flash('Item not found!', 'error');
            return redirect()->route('jobs.index');
        }

        return view('jobs.show')->withJob($job);
    }

    /**
     * Submit job
     *
     * @param $id
     *
     * @return \Illuminate\Http\Response
     */
    public function submit($id)
    {
        $job = $this->jobsService->findUserJobById($id);

        if (!$job) {
            flash('Item not found!', 'error');
            return redirect()->route('jobs.index');
        }

        $response = $this->jobsService->submitJobForModeration($job);

        flash($response['message'], $response['type']);

        return redirect()->route('jobs.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $job = $this->jobsService->findUserJobById($id);

        if (!$job) {
            flash('Item not found!', 'error');
            return redirect()->route('jobs.index');
        }

        if (!$this->jobsService->isEligibleToEdit($job)) {
            flash($job->status . ' job cannot be edit!', 'error');
            return redirect()->route('jobs.index');
        }

        return view('jobs.edit')->withJob($job);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\JobRequest $request
     * @param  int                          $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(JobRequest $request, $id)
    {
        $job = $this->jobsService->findUserJobById($id);

        if (!$job) {
            flash('Item not found!', 'error');
            return redirect()->route('jobs.index');
        }

        $is_updated = $this->jobsService->update($request->except(['_method', '_token', 'files']), $job);

        if ($is_updated) {
            flash('Job updated successfully!');
        } else {
            flash('Failed to update job!', 'error');
        }

        return redirect()->route('jobs.edit', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $is_deleted = $this->jobsService->deleteJob($id);

        if ($is_deleted) {
            flash('Job deleted successfully!');
        } else {
            flash('Failed to delete job!', 'error');
        }

        return redirect()->route('jobs.index');
    }

    /**
     * Approve job
     *
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function approve($id)
    {
        $job = $this->jobsService->findById($id);

        if (!$job) {
            flash('Item not found!', 'error');
            return redirect()->route('jobs.index');
        }

        $response = $this->jobsService->approveJob($job);

        flash($response['message'], $response['type']);

        return redirect()->route('jobs.index');
    }

    /**
     * Mark as spam job
     *
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function spam($id)
    {
        $job = $this->jobsService->findById($id);

        if (!$job) {
            flash('Item not found!', 'error');
            return redirect()->route('jobs.index');
        }

        $response = $this->jobsService->markAsSpamJob($job);

        flash($response['message'], $response['type']);

        return redirect()->route('jobs.index');
    }
}
