<?php

namespace App\Http\Controllers;

use App\Services\JobsService;
use Illuminate\Http\Request;

class PublicController extends Controller
{
    /**
     * @var \App\Services\JobsService
     */
    private $service;

    public function __construct(JobsService $service)
    {
        $this->service = $service;
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $filter = $request->get('filter', []);
        $jobs = $this->service->getPublishedJobs($filter);

        return view('frontend.welcome', compact('jobs', 'filter'));
    }

    /**
     * @param $id
     * @param $slug
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function show($id, $slug)
    {
        $job = $this->service->findByIdAndSlug($id, $slug);

        if (!$job) {
            flash('Job not found!', 'error');
            return redirect()->to('/');
        }

        return view('frontend.jobs.details')->withJob($job);
    }

    /**
     * Approve job
     *
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function approve($id)
    {
        $job = $this->service->findById($id);

        if ($job) {
            $response = $this->service->approveJobFromMail($job);

            flash($response['message'], $response['type']);
        } else {
            flash('Job not found!', 'error');
        }

        return redirect()->to('/');
    }

    /**
     * Mark as spam
     *
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function spam($id)
    {
        $job = $this->service->findById($id);

        if ($job) {
            $response = $this->service->markJobAsSpamFromMail($job);

            flash($response['message'], $response['type']);
        } else {
            flash('Job not found!', 'error');
        }


        return redirect()->to('/');
    }
}
