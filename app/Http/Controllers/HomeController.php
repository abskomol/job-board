<?php

namespace App\Http\Controllers;

use App\Services\DashboardService;

class HomeController extends Controller
{
    /**
     * @var \App\Services\DashboardService
     */
    private $service;

    /**
     * Create a new controller instance.
     *
     * @param \App\Services\DashboardService $service
     */
    public function __construct(DashboardService $service)
    {
        $this->middleware('auth');
        $this->service = $service;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stats = $this->service->getStatus();

        return view('dashboard.home')->withStats($stats);
    }
}
