<?php

namespace App\Jobs;

use App\Job;
use App\Mail\ManagerNotificationEmail;
use App\Mail\ModeratorNotificationEmail;
use App\Support\Constants;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendNotificationEmail implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var \App\Job
     */
    private $jobObject;

    /**
     * Create a new job instance.
     *
     * @param \App\Job $jobObject
     */
    public function __construct(Job $jobObject)
    {
        $this->jobObject = $jobObject;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->isFirstTimePosted()) {
            $this->notifyToManager();
            $this->notifyModerator();
        } else {
            $this->approveJob();
        }

    }

    /**
     * @return bool
     */
    protected function isFirstTimePosted()
    {
        /** @var \App\Repositories\JobsRepository $jobRepository */
        $jobRepository = app('App\Repositories\JobsRepository');

        $query = $jobRepository->getQuery()
            ->where('user_id', $this->jobObject->user_id)
            ->where('email', $this->jobObject->email)
            ->where('status', Constants::$job_approved_status)
            ->whereNotIn('id', [$this->jobObject->id])
            ->get();

        if ($query->count()) {
            return false;
        }

        return true;
    }

    /**
     * Send notification to manager
     */
    private function notifyToManager()
    {
        $user = User::manager()->first();

        if ($user) {
            Mail::to($user->email)
                ->send(new ManagerNotificationEmail());
        }
    }

    /**
     * Send notification to manager
     */
    private function notifyModerator()
    {
        $user = User::moderator()->first();

        if ($user) {
            Mail::to($user->email)
                ->send(new ModeratorNotificationEmail($this->jobObject));
        }
    }

    /**
     * Approve job
     */
    protected function approveJob()
    {
        /** @var \App\Services\JobsService $jobService */
        $jobService = app('App\Services\JobsService');
        $jobService->changeStatusToApprove($this->jobObject);
    }
}
