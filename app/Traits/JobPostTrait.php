<?php namespace App\Traits;

use App\Support\Constants;
use Illuminate\Database\Eloquent\Model;

trait JobPostTrait
{
    public static function bootJobPostTrait()
    {
        static::creating(function(Model $model) {
            $model->slug = str_slug($model->title);
            if (auth()->check()) {
                $model->user_id =  auth()->user()->id;
            }
        });
    }

    /**
     * Return formatted string based on status
     *
     * @return string
     */
    public function statusString()
    {
        $str = '';

        if ($this->status === Constants::$job_draft_status) {
            $str =  '<span class="label label-default pull-right">%s</span>';
        } elseif ($this->status === Constants::$job_approved_status) {
            $str =  '<span class="label label-success pull-right">%s</span>';
        } elseif ($this->status === Constants::$job_spam_status) {
            $str =  '<span class="label label-danger pull-right">%s</span>';
        } elseif ($this->status === Constants::$job_submitted_status) {
            $str =  '<span class="label label-info pull-right">Waiting for approval</span>';
        }

        return sprintf($str, $this->status);
    }
}
