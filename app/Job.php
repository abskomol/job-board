<?php

namespace App;

use App\Traits\JobPostTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class JobPost
 *
 * @package App
 */
class Job extends Model
{
    use SoftDeletes;
    use JobPostTrait;

    /**
     * @var array
     */
    protected $guarded = ['id'];

    protected $dates = ['approved_date'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
