<?php

namespace App\Support;

class Constants
{
    public static $status_list = [
        'DRAFT' => 'Draft',
        'SUBMITTED' => 'Submitted',
        'APPROVED' => 'Approved',
        'SPAM' => 'Spam'
    ];

    public static $job_draft_status = 'DRAFT';
    public static $job_submitted_status = 'SUBMITTED';
    public static $job_approved_status = 'APPROVED';
    public static $job_spam_status = 'SPAM';

    public static $can_delete_status_list = [
        'DRAFT',
        'SPAM'
    ];

    public static $can_edit_status_list = [
        'DRAFT'
    ];
    public static $item_per_page = 10;
}