<?php

/**
 * Set a flash message in the session.
 *
 * @param  string $message
 * @param string $type
 */
function flash($message, $type = 'info') {

    switch($type) {
        case 'error':
            session()->flash('error', $message);
            break;
        default:
            session()->flash('message', $message);
            break;
    }
}


/**
 * Check in user is moderator
 *
 * @param null $user
 *
 * @return bool
 */
function userIsModerator($user = null) {
    if (!auth()->check()) {

        if ($user) {
            return (bool) $user->is_moderator;
        }

        return false;
    }

    if (auth()->user()->is_moderator) {
        return true;
    }

    return false;
}

/**
 * Check current logged in user is manager
 *
 * @return bool
 */
function userIsManager() {
    if (!auth()->check()) {
        return false;
    }

    if (auth()->user()->is_manager) {
        return true;
    }

    return false;
}
